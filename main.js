const axios = require("axios")
const os = require("os")
const irc = require("node-irc")
const fs = require("fs")

if (!fs.existsSync("./config.json")) {
    console.log("No config.json found, please create one!")
    process.exit(1)
}

const config = require("./config.json")

const client = new irc(config.irc.host, config.irc.port, config.irc.nick, config.irc.fullname, config.irc.password, '')

const main = () => {
    client.debug = false
    client.verbosity = config.debug.level
    const banner = fs.readFileSync('./banner.txt', 'utf8')
    console.log(`
${os.platform()}: ${os.release()} | ${os.machine()} | ${os.hostname()}

${banner}
`)

    client.on('ready', () => {
        for (let i = 0; i < config.irc.channels.length; i++) {
            client.join(config.irc.channels[i])
        }
    })

    client.on('PRIVMSG', async (data) => {
        if (data.sender !== client.nickname && data.message.startsWith(config.commands.prefix)) {
            let args = String(data.message).split(' ').slice(1)
            if (args.length !== 1) {
                client.say(data.sender, `Usage: ${config.commands.prefix} <location> (e.g. ${config.commands.prefix} Berlin)`)
                return
            }
            let response = await axios.get(`https://wttr.in/${args[0]}?0&T`, {
                headers: {
                    "User-Agent": "curl/7.64.1",
                },
            })
            let resp = response.data.split('\n')
            for (let i = 0; i < resp.length; i++) {
                await client.say(data.sender, resp[i])
                await new Promise(r => setTimeout(r, config.commands.delay))
            }
        }
    })

    client.connect()
}

main()