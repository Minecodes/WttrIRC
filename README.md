# WttrIRC

An IRC bot for requesting the weather on wttr.in

## Usage

### Server-side

```bash
node main.js
```

### Client-side

`your-prefix` + `your-location` - Request the weather for the given location over wttr.in

## Configuration

The configuration file is `config.json`. It contains the following options:

```json
{
    "irc": {
        "host": "irc.libera.chat",
        "port": 6667,
        "password": "",
        "nick": "ircbot",
        "fullname": "ircbot",
        "channels": [
            "####"
        ]
    },
    "commands": {
        "prefix": "!bot",
        "delay": 1000
    },
    "debug": {
        "level": 0
    }
}
```

### IRC

The `irc` section contains the IRC server configuration.

- `host`: The IRC server host.
- `port`: The IRC server port.
- `password`: The IRC server password of your account.
- `nick`: The nickname of your bot, which is also used for running the command.
- `fullname`: The full name of your bot. You can leave this empty.
- `channels`: The channels your bot should join on start.

### Commands

The `commands` section contains the command configuration.

- `prefix`: The prefix of the command. The command is `prefix` + `command`.
- `delay`: The delay between each message sent by the bot in milliseconds.

### Debug

The `debug` section contains the debug configuration.

- `level`: The debug level. The higher the number, the more debug messages are shown.

## Setup

1. Install Node.js & `screen`
2. Install the dependencies with `npm install` or `yarn`
3. Configure the bot
4. Start the bot with `screen -S wttrirc node main.js`

**!! If you want to stop the bot, use this !!**

1. `screen -r wttrirc`
2. press `CTRL + C`